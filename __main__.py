#!/usr/bin/env python3
"""CLI entry point.

This module parses command line arguments using argparse module.
"""

import sys
import argparse

from imgtools.Image import Image

def main(args):
    """
    The main function of the programme.
    """

    try:
        img = Image(args.filename[0])
    except FileNotFoundError:
        print('File with name `{}` was not found.'.format(args.filename[0]), file=sys.stderr)
        exit(1)

    if args.flip_horizontal:
        img.flip_horizontal()
    if args.flip_vertical:
        img.flip_vertical()
    if args.transpose:
        img.transpose()
    if args.inverse:
        img.inverse()
    if args.greyscale:
        img.greyscale()
    if args.sharpen:
        img.sharpen()

    try:
        if args.lighten:
            img.lighten(int(args.lighten))
        if args.darken:
            img.darken(int(args.darken))
        if args.shrink:
            img.shrink(int(args.shrink))
    except ValueError:
        print('Invalid numeric value.', file=sys.stderr)
        exit(1)
    if args.output:
        img.output(args.output)

    try:
        img.save()
    except ValueError:
        print('Unsuported image format.', file=sys.stderr)
        exit(1)


if __name__ == '__main__':
    # Execult the main funtion iff this module
    # is run as the main programme and setup
    # CLI interface
    parser = argparse.ArgumentParser(description='Simple command line image toolbox.')
    parser.add_argument(
        'filename',
        metavar='filename',
        type=str,
        nargs=1,
        help='Name of a file with relative or absolute path to it.'
    )
    parser.add_argument(
        '-f',
        '--flip-horizontal',
        dest='flip_horizontal',
        action='store_true',
        default=False,
        help='Flips given image horizontally.'
    )
    parser.add_argument(
        '-F',
        '--flip-vertical',
        dest='flip_vertical',
        action='store_true',
        default=False,
        help='Flips given image vertically.'
    )
    parser.add_argument(
        '-t',
        '--transpose',
        dest='transpose',
        action='store_true',
        default=False,
        help='Transposes the whole image.'
    )
    parser.add_argument(
        '-i',
        '--inverse',
        dest='inverse',
        action='store_true',
        default=False,
        help='Inverts colors of an image.'
    )
    parser.add_argument(
        '-g',
        '--greyscale',
        dest='greyscale',
        action='store_true',
        default=False,
        help='Greyscales colors of an image.'
    )
    parser.add_argument(
        '-S',
        '--sharpen',
        dest='sharpen',
        action='store_true',
        default=False,
        help='Sharpens the image.'
    )
    parser.add_argument(
        '-l',
        '--lighten',
        metavar='<percentage>',
        dest='lighten',
        action='store',
        help='Increeses the brighteness of an image by <percentage>.'
    )
    parser.add_argument(
        '-d',
        '--darken',
        metavar='<percentage>',
        dest='darken',
        action='store',
        help='Idecreases the brighteness of an image by <percentage>.'
    )
    parser.add_argument(
        '-s',
        '--shrink',
        metavar='<percentage>',
        dest='shrink',
        action='store',
        help='Shrinks the image by <coefficient>.'
    )
    parser.add_argument(
        '-o',
        '--output',
        metavar='<file>',
        dest='output',
        action='store',
        help='Write the output image to <file>.'
    )

    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version='1.0.0',
        help='Prints program\'s version number and exits.'
    )

    main(parser.parse_args())
