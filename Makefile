IMAGES = @(shell ls  | grep -v 'kve')

clean:
	# Clear all precompiled files
	find . -type f -name '*.pyc' -exec rm '{}' +
	# Remove all cached directories
	find . -type d -name '__pycache__' -exec rmdir '{}' +
	# Remove all images except the example one.
	-rm -rf img_out.jpg
