"""Image
Module Image contains declaration of the Image class
and all other functions and classes that are intented
to be used by or with the Image class and/or objects.
"""

import numpy as np
from PIL import Image as _Image

class Image(object):
    def __init__(self, path):
        """
        Initializes an Image object to default values.
        """
        self.in_path  = path
        self.data     = None
        self.out_path = './img_out.jpg'
        self.colors   = 'RGB'
        self._load()

    def _load(self):
        """
        Loads a image with provided path and converts it to data form.
        """
        self.data = np.asarray(_Image.open(self.in_path), dtype=np.int32)

    def flip_horizontal(self):
        """
        Flips the image horizontally.
        """
        self.data = self.data[::-1]

    def flip_vertical(self):
        """
        Flips the image vetically.
        """
        self.data = self.data[:, ::-1]

    def transpose(self):
        """
        Transpose the image.
        """
        self.data = self.data.swapaxes(0, 1)

    def inverse(self):
        """
        Inverses the colors of the image.
        """
        self.data = 255 - self.data

    def greyscale(self):
        """
        Converts the image to greyscale.
        """
        self.colors = 'L'
        # Broadcasting
        self.data = [0.229, 0.587, 0.114] * self.data
        self.data = self.data.sum(axis=2)
        self.data = np.asarray(self.data, dtype=np.uint8)

    def sharpen(self):
        """
        Sharpens the image.
        """
        self.data = (
            - self.data[0:-2, 0:-2] -     self.data[0:-2, 1:-1] - self.data[0:-2, 2:]
            - self.data[1:-1, 0:-2] + 9 * self.data[1:-1, 1:-1] - self.data[1:-1, 2:]
            - self.data[2:, 0:-2]   -     self.data[2:, 1:-1]   - self.data[2:, 2:]
        )
        self.data = np.clip(self.data, 0, 255)
        self.data = np.asarray(self.data, dtype=np.uint8)

    def lighten(self, percentage):
        """
        Lightens the image by the percentage.
        """
        self.data = self.data + self.data * (percentage / 100)
        self.data = np.clip(self.data, 0, 255)
        self.data = np.asarray(self.data, dtype=np.uint8)

    def darken(self, percentage):
        """
        Darkens the image by the percentage.
        """
        self.data = self.data - self.data * (percentage / 100)
        self.data = np.clip(self.data, 0, 255)
        self.data = np.asarray(self.data, dtype=np.uint8)

    def shrink(self, coef):
        """
        Shrinks the image by the factor.
        """
        self.data = self.data[::coef, ::coef]

    def output(self, out_path):
        """
        Sets the output path of the image.
        """
        self.out_path = out_path

    def save(self):
        """
        Saves the currently open image as image with given name.
        """
        self.data = np.asarray(self.data, dtype=np.uint8)
        _Image.fromarray(self.data, self.colors).save(self.out_path)
